<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Trip extends Model
{
    //
    protected $guarded = ['id'];
    protected $hidden = ['pivot'];

    //relations
    public  function bus()
    {
        return $this->belongsTo(Bus::class , 'bus_id');
    }

    public  function governorates()
    {
        return $this->belongsToMany(Governorate::class , 'trip_governorates','trip_id','governorate_id')->withPivot('order')->withTimestamps()->orderBy('order','ASC');
    }



}
