<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bus extends Model
{
    //
    protected $guarded = ['id'];

    //relations
    public  function seats()
    {
        return $this->hasMany(BusSeat::class , 'bus_id');
    }


}
