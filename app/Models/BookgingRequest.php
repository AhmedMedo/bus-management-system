<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BookgingRequest extends Model
{
    //
    protected $guarded = ['id'];

    public function trip()
    {
        return $this->belongsTo(Trip::class , 'trip_id');
    }

    public function FromGovernorate()
    {
        return $this->belongsTo(Governorate::class , 'from_governorate_id');

    }

    public function ToGovernorate()
    {
        return $this->belongsTo(Governorate::class , 'to_governorate_id');

    }

    public function BusSeat()
    {
        return $this->belongsTo(BusSeat::class , 'bus_seat_id');

    }



    public static function GetAvailableSeats($trip_id , $from_governorate_id , $to_governorate_id)
    {
        $trip = Trip::find($trip_id);
        $trip_governorate = TripGovernorate::where('trip_id',$trip_id)->where('governorate_id',$from_governorate_id)->first();
        $previous_governorates = TripGovernorate::where('trip_id',$trip_id)->where('order','<=',$trip_governorate->order)->pluck('governorate_id')->toArray(); 
        $booked_seats_ids = static::query()->where('trip_id',$trip_id)->whereIn('from_governorate_id',$previous_governorates)->where('to_governorate_id',$to_governorate_id)->pluck('bus_seat_id')->toArray();
        $availabel_seats = BusSeat::where('bus_id',$trip->bus_id)->whereNotIn('id',$booked_seats_ids)->get();
        return $availabel_seats;
    }

}
