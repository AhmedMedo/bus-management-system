<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TripResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'code' => $this->code,
            'price' =>$this->price,
            'date' =>$this->date,
            'bus' =>$this->bus,
            'governorates' =>$this->governorates,
            'created_at' =>$this->created_at->toDateTimeString()
        ];
    }
}
