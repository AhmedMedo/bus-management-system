<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\TripResource;
use App\Models\Trip;
use App\Http\Requests\AvailableSeatRequest;
use App\Http\Requests\BookingSeatRequest;
use App\Models\BookgingRequest;
class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function trips()
    {
        //
        return TripResource::collection(Trip::orderBy('created_at','DESC')->paginate(10));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function available_seats(AvailableSeatRequest $request)
    {
        //
        return response()->json(['data'=>BookgingRequest::GetAvailableSeats($request->trip_id , $request->start_governorate_id,$request->end_governorate_id)]);
    }

    /**
     * Book seat.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function book_seat(BookingSeatRequest $request)
    {
        //
        // $request->request()->add(['user_id'=>$request->user()->id]);
        BookgingRequest::firstOrcreate($request->all() + ['user_id' => $request->user()->id]);
        return response()->json(['message'=>'request booked successfully']);

    }

}
