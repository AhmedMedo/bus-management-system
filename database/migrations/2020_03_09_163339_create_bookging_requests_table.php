<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookgingRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookging_requests', function (Blueprint $table) {
            $table->id();
            $table->integer('trip_id');
            $table->integer('from_governorate_id');
            $table->integer('to_governorate_id');
            $table->integer('bus_seat_id');
            $table->integer('user_id');
            $table->enum('status',['pending','paid','refunded'])->default('paid')->comment('pending - paid - refunded');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookging_requests');
    }
}
