<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Trip;
use Faker\Generator as Faker;

$factory->define(Trip::class, function (Faker $faker) {
    return [
        //
        'code' =>$faker->randomNumber('5'),
        'date' => $faker->dateTimeBetween('now','+30 days'),
        'price' => $faker->randomNumber(2),
        'bus_id' => function(){
                    return App\Models\Bus::inRandomOrder()->first()->id;
        }
    ];
});
