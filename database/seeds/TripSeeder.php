<?php

use Illuminate\Database\Seeder;
use App\Models\Trip;

class TripSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // we start with to trips now
        $trips = factory(App\Models\Trip::class, 2)->create();


    }
}
