<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(GovernorateSeeder::class);
        $this->call(BusSeeder::class);
        $this->call(BusSeatsSeeder::class);
        $this->call(TripSeeder::class);
        $this->call(TripGovernorateSeeder::class);
    }
}
