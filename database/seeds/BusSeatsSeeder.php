<?php

use Illuminate\Database\Seeder;
use App\Models\Bus;
use App\Models\BusSeat;
class BusSeatsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $buses = Bus::all();
        foreach ($buses as $bus)
        {
            for ($i = 1 ; $i<= $bus->number_of_seats ; $i++)
            {
                $bus->seats()->create([
                    'seat_code' => $bus->code.'_'.$i
                ]);
            }

        }

    }
}
