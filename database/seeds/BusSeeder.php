<?php

use Illuminate\Database\Seeder;
use App\Models\Bus;
use Illuminate\Support\Str;
class BusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        for($i = 0 ; $i<=10 ; $i++)
        {
            Bus::firstOrCreate(['code' =>Str::random(4),'number_of_seats' => 12]);
        }
    }
}
