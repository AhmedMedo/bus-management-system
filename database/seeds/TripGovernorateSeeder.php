<?php

use Illuminate\Database\Seeder;
use App\Models\Trip;
use App\Models\Governorate;
class TripGovernorateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $first_trip = Trip::find(1);
        $second_trip = Trip::find(2);

        //Assume First Trip will be Cario -> Fayoum ->Minya ->Aswan

        $first_trip->governorates()->attach(1,['order'=> 1]); //Cairo
        $first_trip->governorates()->attach(7,['order'=> 2]); // Fayoum
        $first_trip->governorates()->attach(11,['order'=> 3]); // Minya
        $first_trip->governorates()->attach(16,['order'=> 4]); // Aswan

        //Assume Second Trip will be Alexandria -> Gharbiya ->Cairo ->Giza

        $second_trip->governorates()->attach(3,['order'=> 1]); //Alexandria
        $second_trip->governorates()->attach(8,['order'=> 2]); // Gharbiya
        $second_trip->governorates()->attach(1,['order'=> 3]); // Cairo
        $second_trip->governorates()->attach(3,['order'=> 4]); // Giza






    }
}
