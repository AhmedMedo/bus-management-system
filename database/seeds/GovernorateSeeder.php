<?php

use Illuminate\Database\Seeder;
use App\Models\Governorate;
class GovernorateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $governorates = array(
            array('name' => 'القاهرة','name_en' => 'Cairo'),
            array('name' => 'الجيزة','name_en' => 'Giza'),
            array('name' => 'الأسكندرية','name_en' => 'Alexandria'),
            array('name' => 'الدقهلية','name_en' => 'Dakahlia'),
            array('name' => 'البحر الأحمر','name_en' => 'Red Sea'),
            array('name' => 'البحيرة','name_en' => 'Beheira'),
            array('name' => 'الفيوم','name_en' => 'Fayoum'),
            array('name' => 'الغربية','name_en' => 'Gharbiya'),
            array('name' => 'الإسماعلية','name_en' => 'Ismailia'),
            array('name' => 'المنوفية','name_en' => 'Monofia'),
            array('name' => 'المنيا','name_en' => 'Minya'),
            array('name' => 'القليوبية','name_en' => 'Qaliubiya'),
            array('name' => 'الوادي الجديد','name_en' => 'New Valley'),
            array('name' => 'السويس','name_en' => 'Suez'),
            array('name' => 'اسوان','name_en' => 'Aswan'),
            array('name' => 'اسيوط','name_en' => 'Assiut'),
            array('name' => 'بني سويف','name_en' => 'Beni Suef'),
            array('name' => 'بورسعيد','name_en' => 'Port Said'),
            array('name' => 'دمياط','name_en' => 'Damietta'),
            array('name' => 'الشرقية','name_en' => 'Sharkia'),
            array('name' => 'جنوب سيناء','name_en' => 'South Sinai'),
            array('name' => 'كفر الشيخ','name_en' => 'Kafr Al sheikh'),
            array('name' => 'مطروح','name_en' => 'Matrouh'),
            array('name' => 'الأقصر','name_en' => 'Luxor'),
            array('name' => 'قنا','name_en' => 'Qena'),
            array('name' => 'شمال سيناء','name_en' => 'North Sinai'),
            array('name' => 'سوهاج','name_en' => 'Sohag')
        );
        foreach ($governorates as $gov)
        {
            Governorate::firstOrCreate($gov);
        }


    }
}
